// ====================================================================
// This file is part of FlexibleSUSY.
//
// FlexibleSUSY is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// FlexibleSUSY is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with FlexibleSUSY.  If not, see
// <http://www.gnu.org/licenses/>.
// ====================================================================

// File generated at @DateAndTime@

#ifndef @ModelName@_SPECTRUM_GENERATOR_H
#define @ModelName@_SPECTRUM_GENERATOR_H

#include "@ModelName@_spectrum_generator_interface.hpp"
#include "@ModelName@_two_scale_high_scale_constraint.hpp"
#include "@ModelName@_two_scale_susy_scale_constraint.hpp"
#include "@ModelName@_two_scale_low_scale_constraint.hpp"
#include "@ModelName@_two_scale_convergence_tester.hpp"
#include "@ModelName@_two_scale_initial_guesser.hpp"
#include "@ModelName@_utilities.hpp"
#include "@ModelName@_SM_two_scale_matching.hpp"
#include "@ModelName@_input_parameters.hpp"

#include "lowe.h"
#include "error.hpp"
#include "numerics2.hpp"
#include "two_scale_running_precision.hpp"
#include "two_scale_solver.hpp"
#include "SM_two_scale_model.hpp"
#include "ew_input.hpp"

#include <limits>

namespace flexiblesusy {

template <class T>
class @ModelName@_spectrum_generator
   : public @ModelName@_spectrum_generator_interface<T> {
public:
   @ModelName@_spectrum_generator()
      : @ModelName@_spectrum_generator_interface<T>()
      , solver()
      , high_scale_constraint()
      , susy_scale_constraint()
      , low_scale_constraint()
      , high_scale(0.)
      , susy_scale(0.)
      , low_scale(0.)
   {}
   virtual ~@ModelName@_spectrum_generator() {}

   double get_high_scale() const { return high_scale; }
   double get_susy_scale() const { return susy_scale; }
   double get_low_scale()  const { return low_scale;  }
   virtual void run(const softsusy::QedQcd&, const @ModelName@_input_parameters&);
   void write_running_couplings(const std::string& filename = "@ModelName@_rgflow.dat") const;
   void do2LoopLogCalc();
   void calcHiggsPoleEFT();

private:
   RGFlow<T> solver;
   @ModelName@_high_scale_constraint<T> high_scale_constraint;
   @ModelName@_susy_scale_constraint<T> susy_scale_constraint;
   @ModelName@_low_scale_constraint<T>  low_scale_constraint;
   double high_scale, susy_scale, low_scale;
};

/**
 * @brief Run's the RG solver with the given input parameters
 *
 * This function sets up the RG solver using a high-scale, susy-scale
 * and low-scale constraint.  Afterwards the solver is run until
 * convergence is reached or an error occours.  Finally the particle
 * spectrum (pole masses) is calculated.
 *
 * @param oneset Standard Model input parameters
 * @param input model input parameters
 */
template <class T>
void @ModelName@_spectrum_generator<T>::run(const softsusy::QedQcd& oneset,
                                const @ModelName@_input_parameters& input)
{
   @ModelName@<T>& model = this->model;
   model.clear();
   model.set_input_parameters(input);
   model.do_calculate_sm_pole_masses(this->calculate_sm_masses);
   model.do_force_output(this->force_output);
   model.set_loops(this->beta_loop_order);
   model.set_thresholds(this->threshold_corrections_loop_order);
   model.set_zero_threshold(this->beta_zero_threshold);

   high_scale_constraint.clear();
   susy_scale_constraint.clear();
   low_scale_constraint .clear();

   // needed for constraint::initialize()
   high_scale_constraint.set_model(&model);
   susy_scale_constraint.set_model(&model);
   low_scale_constraint .set_model(&model);

   low_scale_constraint .set_sm_parameters(oneset);

   high_scale_constraint.initialize();
   susy_scale_constraint.initialize();
   low_scale_constraint .initialize();

   std::vector<Constraint<T>*> upward_constraints(2);
   upward_constraints[0] = &low_scale_constraint;
   upward_constraints[1] = &high_scale_constraint;

   std::vector<Constraint<T>*> downward_constraints(3);
   downward_constraints[0] = &high_scale_constraint;
   downward_constraints[1] = &susy_scale_constraint;
   downward_constraints[2] = &low_scale_constraint;

   @ModelName@_convergence_tester<T> convergence_tester(&model, this->precision_goal);
   if (this->max_iterations > 0)
      convergence_tester.set_max_iterations(this->max_iterations);

   @ModelName@_initial_guesser<T> initial_guesser(&model, oneset,
                                                  low_scale_constraint,
                                                  susy_scale_constraint,
                                                  high_scale_constraint);

   Two_scale_increasing_precision precision(10.0, this->precision_goal);

   solver.reset();
   solver.set_convergence_tester(&convergence_tester);
   solver.set_running_precision(&precision);
   solver.set_initial_guesser(&initial_guesser);
   solver.add_model(&model, upward_constraints, downward_constraints);

   high_scale = susy_scale = low_scale = 0.;
   this->reached_precision = std::numeric_limits<double>::infinity();

   try {
      solver.solve();
      high_scale = high_scale_constraint.get_scale();
      susy_scale = susy_scale_constraint.get_scale();
      low_scale  = low_scale_constraint.get_scale();
      this->reached_precision = convergence_tester.get_current_accuracy();

      model.run_to(susy_scale);
      model.solve_ewsb();
      model.calculate_spectrum();

      // copy calculated W pole mass
      model.get_physical().M@VectorW@
         = low_scale_constraint.get_sm_parameters().displayPoleMW();

      // start logarithmic resummation
      switch (model.get_Two_loop_corrections().higgs_log)
      {
         case 0: break;
         case 1: do2LoopLogCalc(); break;
         case 2: calcHiggsPoleEFT(); break;
      }

      // run to output scale (if scale > 0)
      if (!is_zero(this->parameter_output_scale)) {
         model.run_to(this->parameter_output_scale);
      }
   } catch (const NoConvergenceError&) {
      model.get_problems().flag_no_convergence();
   } catch (const NonPerturbativeRunningError&) {
      model.get_problems().flag_no_perturbative();
   } catch (const NoRhoConvergenceError&) {
      model.get_problems().flag_no_rho_convergence();
   } catch (const Error& error) {
      model.get_problems().flag_thrown(error.what());
   } catch (const std::string& str) {
      model.get_problems().flag_thrown(str);
   } catch (const char* str) {
      model.get_problems().flag_thrown(str);
   } catch (const std::exception& error) {
      model.get_problems().flag_thrown(error.what());
   }
}

/**
 * Create a text file which contains the values of all model
 * parameters at all scales between the low-scale and the high-scale.
 *
 * @param filename name of output file
 */
template <class T>
void @ModelName@_spectrum_generator<T>::write_running_couplings(
   const std::string& filename) const
{
   @ModelName@_spectrum_generator_interface<T>::write_running_couplings(filename, low_scale, high_scale);
}

/**
 * Calculates logarithmic contributions to Higgs mass of 2-loop order and higher
 */
template <class T>
void @ModelName@_spectrum_generator<T>::do2LoopLogCalc()
{

  /*
   * SM1: matching TL+1L+ running all orders with 2-Loop betafunctions, ewsb_loop_order = 1  => for observable TL+1L
   * SM2: matching and running TL, ewsb_loop_order = 1        => for observable TL+1L correction: subtracts TL + 1L-self-energy
   * SM3: matching TL, running 1L (only), ewsb_loop_order = 0 => for observable TL correction: subtracts 1L-contributions from running
   * SM4: matching TL+1L, running TL, ewsb_loop_order = 0     => for observable TL correction: subtracts 1L-contributions from matching [deleted: replaced with SM4_Mhh2]
   */

  @ModelName@<T>& model = this->model;
  double  p, p0, M_Pole, M_Higgs,  SM3_Mhh2, SM4_Mhh2;
  M_Higgs = @GetHiggsMass@;
  M_Pole = 0.;

  SM<T>  SM1, SM2, SM3;
  SM1.set_ewsb_loop_order(1);	SM1.set_pole_mass_loop_order(1);
  SM2.set_ewsb_loop_order(1);	SM2.set_pole_mass_loop_order(1);
  SM3.set_ewsb_loop_order(1);	SM3.set_pole_mass_loop_order(1);

  //beta loop order is equal to the parent model
  SM1.set_loops(model.get_loops());

  //matching
  model.calculate_DRbar_masses();
  model.solve_ewsb_one_loop();
  @ModelName@_SM_two_scale_matching<T>::matching(SM1, model);
  @ModelName@_SM_two_scale_matching<T>::matchingTL(SM2, model);
  SM3 = SM2;
  SM4_Mhh2 = @ModelName@_SM_two_scale_matching<T>::Mhh2matching1L(model);

  //solve ewsb-equations
  SM1.solve_ewsb_one_loop();
  SM2.solve_ewsb_one_loop();
  SM3.solve_ewsb_tree_level();

  //running
  SM1.run_to(low_scale);
  SM2.set_scale(low_scale);
  //extract 1-loop part of SM3-running
  SM3.set_loops(1);
  SM3.set(SM3.beta()*FiniteLog(low_scale/SM3.get_scale()));
  SM3.set_scale(low_scale);
  //since SM4 runs with TL in the SM (only scale changes),
  //but gives only TL correcture (no scale parameter), it is not affected by running

  //calculate parameter-masses
  SM1.calculate_DRbar_masses();
  SM2.calculate_DRbar_masses();

  //recalculate mu2 with 1-loop EWSB  condition at proper scale
  SM1.solve_ewsb_one_loop();

  SM3_Mhh2 = -SM3.get_mu2() + 1.5*SM3.get_Lambdax()*Sqr(SM2.get_v()) + 3.*SM2.get_Lambdax()*SM2.get_v()*SM3.get_v();
  SM4_Mhh2 -= Sqr(SM2.get_Mhh());


@DiagonalizeEFT@

  @GetHiggsMass@ = M_Higgs;
}

/**
 * Calculates Higgs pole mass in the SM as effective field theory for the full theory,
 * using SM input parameters at the scale MZ for the VEV, yukawas and gauge couplings
 */
template <class T>
void @ModelName@_spectrum_generator<T>::calcHiggsPoleEFT()
{

   @ModelName@<T> model = this->model;
   Eigen::Matrix<double, 3, 3> ZeroMatrix = Eigen::Matrix<double, 3, 3>::Zero();
   QedQcd lowEnergyData = low_scale_constraint.get_sm_parameters();
   double lambda_low;
   unsigned iteration = 0;

   SM<T> EFT;
   EFT.set_loops(3);
   EFT.set_ewsb_loop_order(model.get_ewsb_loop_order());
   EFT.set_pole_mass_loop_order(model.get_pole_mass_loop_order());
   EFT.set_precision(model.get_precision());
   EFT.set_ewsb_iteration_precision(model.get_ewsb_iteration_precision());
   EFT.set_number_of_ewsb_iterations(model.get_number_of_ewsb_iterations());
   EFT.set_number_of_mass_iterations(model.get_number_of_mass_iterations());

   const double MZpole = lowEnergyData.displayPoleMZ();
   const double MWpole = lowEnergyData.displayPoleMZ();
   const double MTpole = lowEnergyData.displayPoleMt();
   const double MHpole = 125.09;
   const double alphaS = lowEnergyData.displayAlpha(ALPHAS);
   const double GFermi = lowEnergyData.displayFermiConstant();

   // high precision values from arXiv:1307.3536
   lowEnergyData.runto(MTpole);
   EFT.set_scale(MTpole);
   EFT.set_g1(Sqrt(5./3.) * (.3583 + .00011*(MTpole - 173.34) - .0002*(MWpole - 80.384)/.014));
   EFT.set_g2(.64779 + .00004*(MTpole - 173.34) - .0002*(MWpole - 80.384)/.014);
   EFT.set_g3(1.1666 + .00314*(alphaS - .1184) - .00046*(MTpole - 173.34));
   EFT.set_Yu(ZeroMatrix);
   EFT.set_Yd(ZeroMatrix);
   EFT.set_Ye(ZeroMatrix);
   EFT.set_Yu(2, 2, -(.9369 + .00556*(MTpole - 173.34) - .00042*(alphaS - .1184)/.0007));

   // see (22) from arXiv:1205.6497 [hep-ph]
   EFT.set_v(Sqrt( 1./(Sqrt(2.)*lowEnergyData.displayFermiConstant())
                     + oneOver16PiSqr*( 3.*Sqr(MTpole)*(4.*FiniteLog(MTpole/MTpole) - 1.) + Sqr(MWpole)*(5. - 12.*FiniteLog(MWpole/MTpole))
                     +  2.5*Sqr(MZpole) - 6.*Sqr(MZpole)*FiniteLog(MZpole/MTpole) + 1.5*Sqr(MWpole*MZpole)/(Sqr(MZpole) - Sqr(MWpole))*FiniteLog(MZpole/MWpole) - .5*Sqr(MHpole)
                     -  6.*Sqr(MWpole*MHpole)/(Sqr(MWpole) - Sqr(MHpole))*FiniteLog(MWpole/MHpole))));

   EFT.set_Yu(1, 1, -Sqrt(2.)*lowEnergyData.displayMass(mCharm)/EFT.get_v());
   EFT.set_Yu(0, 0, -Sqrt(2.)*lowEnergyData.displayMass(mUp)/EFT.get_v());
   EFT.set_Yd(2, 2,  Sqrt(2.)*lowEnergyData.displayMass(mBottom)/EFT.get_v());
   EFT.set_Yd(1, 1,  Sqrt(2.)*lowEnergyData.displayMass(mStrange)/EFT.get_v());
   EFT.set_Yd(0, 0,  Sqrt(2.)*lowEnergyData.displayMass(mDown)/EFT.get_v());
   EFT.set_Ye(2, 2,  Sqrt(2.)*lowEnergyData.displayMass(mTau)/EFT.get_v());
   EFT.set_Ye(1, 1,  Sqrt(2.)*lowEnergyData.displayMass(mMuon)/EFT.get_v());
   EFT.set_Ye(0, 0,  Sqrt(2.)*lowEnergyData.displayMass(mElectron)/EFT.get_v());

   // initial value for lambda, see (55) from arXiv:1307.3536 [hep-ph]:
   EFT.set_Lambdax(.12604 + .00206*(MHpole - 125.15) - .00004*(MTpole - 173.34));

   const auto lowEnergyInput = EFT.get();

   EFT.run_to(model.get_scale());
   model.calculate_DRbar_masses();
   model.solve_ewsb_one_loop();
   
   const auto alpha_em = @alphaEMdef@;
   const auto currentScale = model.get_scale();
@em1Linit@
@alphaEM1Lmatching@
   const double MH2matched = @ModelName@_SM_two_scale_matching<T>::Mhh2matching1L(model);
   const double MW2matched = @ModelName@_SM_two_scale_matching<T>::MVWp2matching1L(model);
   const double MZ2matched = @ModelName@_SM_two_scale_matching<T>::MVZ2matching1L(model);
   const double lambda_high = Pi * MH2matched/MW2matched * alpha_em/((1. - MW2matched/MZ2matched)*(1. + delta_alpha_em));
   EFT.run_to(MTpole);

  do
  {
    iteration++;
    // save lambda while other couplings are reset to low scale values
    lambda_low = EFT.get_Lambdax();
    EFT.set(lowEnergyInput);
    EFT.set_Lambdax(lambda_low);
    EFT.run_to(model.get_scale());
    EFT.set_Lambdax(lambda_high);
    EFT.run_to(MTpole);
  }
  while ((MaxRelDiff(lambda_low, EFT.get_Lambdax()) > model.get_precision()
          || MaxRelDiff(lowEnergyInput(32), EFT.get_v()) > model.get_precision()) &&
           iteration < model.get_number_of_mass_iterations());

  //apply SM constraint one last time and calculate pole mass
  lambda_low = EFT.get_Lambdax();
  EFT.set(lowEnergyInput);
  EFT.set_Lambdax(lambda_low);
  EFT.run_to(low_scale);
  EFT.calculate_DRbar_masses();
  EFT.solve_ewsb();
  EFT.calculate_Mhh_pole();

  //store Higgs pole mass in physical structure
  this->@GetHiggsMass@ = EFT.get_physical().Mhh;

}

} // namespace flexiblesusy

#endif
