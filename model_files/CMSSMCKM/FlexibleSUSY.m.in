
FSModelName = "@CLASSNAME@";
FSEigenstates = SARAH`EWSB;
FSDefaultSARAHModel = "MSSM";

(* CMSSM input parameters *)

MINPAR = {
    {3, TanBeta},
    {4, Sign[\[Mu]]}
};

EXTPAR = {
    {21, mHd2IN},
    {22, mHu2IN}
};

RealParameters = { B[\[Mu]], \[Mu], MassB, MassWB, MassG };

EWSBOutputParameters = { B[\[Mu]], \[Mu] };

SUSYScale = Sqrt[Product[M[Su[i]]^(Abs[ZU[i,3]]^2 + Abs[ZU[i,6]]^2), {i,6}]];

SUSYScaleFirstGuess = LowEnergyConstant[MZ];

SUSYScaleInput = {};

HighScale = g1 == g2;

HighScaleFirstGuess = 2.0 10^16;

HighScaleInput = {
   {T[Ye], MatMul[Tp[ZER], Tp[LHInput[T[Ye]]], ZEL]},
   {T[Yd], MatMul[Tp[ZDR], Tp[LHInput[T[Yd]]], ZDL]},
   {T[Yu], MatMul[Tp[ZUR], Tp[LHInput[T[Yu]]], ZUL]},
   {mHd2, mHd2IN},
   {mHu2, mHu2IN},
   {mq2, MatMul[Tp[ZDR], LHInput[mq2], ZDR]},
   {ml2, MatMul[Tp[ZER], LHInput[ml2], ZER]},
   {md2, MatMul[Tp[ZDL], Tp[LHInput[md2]], ZDL]},
   {mu2, MatMul[Tp[ZUL], Tp[LHInput[mu2]], ZUL]},
   {me2, MatMul[Tp[ZEL], Tp[LHInput[me2]], ZEL]},
   {MassB, LHInput[MassB]},
   {MassWB,LHInput[MassWB]},
   {MassG, LHInput[MassG]}
};

LowScale = LowEnergyConstant[MZ];

LowScaleFirstGuess = LowEnergyConstant[MZ];

LowScaleInput = {
   {Yu, Sqrt[2] MatMul[Tp[CKM], topDRbar] / vu},
   {Yd, Sqrt[2] bottomDRbar / vd},
   {Ye, Sqrt[2] electronDRbar / vd},
   {vd, 2 MZDRbar / Sqrt[GUTNormalization[g1]^2 g1^2 + g2^2] Cos[ArcTan[TanBeta]]},
   {vu, 2 MZDRbar / Sqrt[GUTNormalization[g1]^2 g1^2 + g2^2] Sin[ArcTan[TanBeta]]}
};

InitialGuessAtLowScale = {
   {vd, LowEnergyConstant[vev] Cos[ArcTan[TanBeta]]},
   {vu, LowEnergyConstant[vev] Sin[ArcTan[TanBeta]]},
   {Yu, Automatic},
   {Yd, Automatic},
   {Ye, Automatic}
};

InitialGuessAtHighScale = {
   {\[Mu]   , 1.0},
   {B[\[Mu]], 0.0}
};

UseHiggs2LoopMSSM = True;
EffectiveMu = \[Mu];

PotentialLSPParticles = { Chi, Sv, Su, Sd, Se, Cha, Glu };

DefaultPoleMassPrecision = MediumPrecision;
HighPoleMassPrecision    = {hh, Ah, Hpm};
MediumPoleMassPrecision  = {};
LowPoleMassPrecision     = {};

ExtraSLHAOutputBlocks = {
   {ALPHA, {{ArcSin[Pole[ZH[2,2]]]}}},
   {HMIX , {{1, \[Mu]},
            {2, vu / vd},
            {3, Sqrt[vu^2 + vd^2]},
            {4, M[Ah[2]]^2},
            {101, B[\[Mu]]},
            {102, vd},
            {103, vu} } }
};
